<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include('meta.php'); ?>
<link href="/assets/css/style.min.css" rel="stylesheet">
<script src="/assets/js/jquery-3.3.1.min.js"></script>
<script src="/assets/js/jquery-migrate-3.0.1.min.js"></script>
</head>
<body class="page-<?php echo $pageid; ?>">
   <div class="c-header">
      <h1 class="c-logo"><a href="../">Logo</a></h1>

      <nav class="c-Menu">
         <ul>
            <li><a href="../company/">Company</a></li>
            <li><a href="../company/service.php">Service</a></li>
            <li><a href="../news/">News</a></li>
            <li><a href="../contact/">Contact</a></li>
         </ul>
      </nav>
   </div>

